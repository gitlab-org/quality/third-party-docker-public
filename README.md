# A collection of Dockerfiles

Due to licensing restrictions, the QA suite cannot distribute all third party Docker images.
This repository is for distributing the Dockerfiles so they can be built independently of the QA suite.

# Mirroring

For automatically building images on gitlab.com that are up to date with the QA Framework, a CI configuration is provided.
After forking this repository into a private project:

1. Create a [project deploy key](https://docs.gitlab.com/ee/user/project/deploy_keys/index.html#create-a-project-deploy-key) for the fork with write repository permissions
2. Populate the following variables in forked project under Settings > CI/CD > Variables
  * `DEPLOY_PRIVATE_KEY` - the private ssh key for the project deploy key pair
  * `GIT_USER_NAME` - the git username for the git config
  * `GIT_USER_EMAIL` - the git email for the git config
3. (optionally) Create a pipeline schedule for the fork under CI/CD > Schedules with a `daily interval`
  * Note: the scheduled job in the fork will poll the public project and push the changes to the fork.
  * Another job will detect changes in the repository and build/publish the Docker images if needed.

The pipeline can also be triggered manually from CI/CD > Pipelines > Run Pipeline 

### Note: Manually updating the fork will make it out of sync with this project and will break the mirroring.

# Usage

1. When running `gitlab-qa`, provide the following environment variables:
  * `QA_THIRD_PARTY_DOCKER_REGISTRY` (the container registry where the repository/images are hosted, eg registry.gitlab.com)
  * `QA_THIRD_PARTY_DOCKER_REPOSITORY` (the base repository path where the images are hosted, eg registry.gitlab.com/gitlab-org/quality/third-party-docker-private) 
  * `QA_THIRD_PARTY_DOCKER_USER` (a username that has access to the container registry for this repository)
  * `QA_THIRD_PARTY_DOCKER_PASSWORD` (a password/token for the username to authenticate with)

For authenticating when using `gitlab-qa`, consider using a [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html)
