## Jenkins

This project contains the Dockerfile to

* build with default credentials
* skip setup
* install the GitLab Jenkins plugin
* run ruby in Jenkins pipelines

## To build

```shell
docker build -t registry.gitlab.com/<third_party_docker_path>/jenkins:latest
docker push registry.gitlab.com/<third_party_docker_path>/jenkins:latest

```

## Usage

```
docker run -e JENKINS_USER=<admin_user> -e JENKINS_PASS=<admin_password> -p 8080:8080 registry.gitlab.com/<third_party_docker_path>/jenkins:latest
```
